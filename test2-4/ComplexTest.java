import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(5.0, 6.0);
    Complex b = new Complex(-3.0, 4.0);

    @Test
    public void testgetRealPart() {
        assertEquals(5.0,a.getRealPart());
        assertEquals(-3.0,b.getRealPart());
    }

    @Test
    public void testgetImagePart() {
        assertEquals(6.0,a.getImagePart());
        assertEquals(4.0,b.getImagePart());
    }

    @Test
    public void testtoString() {
        assertEquals("Complex{RealPart=5.0, ImagePart=6.0}",a.toString());
        assertEquals("Complex{RealPart=-3.0, ImagePart=4.0}",b.toString());
    }

    @Test
    public void testequals() {
        assertEquals(false,a.equals(b));
        Complex c = new Complex(a.getRealPart(),a.getImagePart());
        assertEquals(true,a.equals(c));
    }

    @Test
    public void testComplexAdd() {
        assertEquals(2.0,a.ComplexAdd(b).getRealPart());
        assertEquals(10.0,a.ComplexAdd(b).getImagePart());
    }

    @Test
    public void testComplexSub() {
        assertEquals(8.0,a.ComplexSub(b).getRealPart());
        assertEquals(2.0,a.ComplexSub(b).getImagePart());
    }

    @Test
    public void testComplexMulti() {
        assertEquals(-39.0,a.ComplexMulti(b).getRealPart());
        assertEquals(2.0,a.ComplexMulti(b).getImagePart());
    }

    @Test
    public void testComplexDiv() {
        assertEquals(0.36,a.ComplexDiv(b).getRealPart());
        assertEquals(-1.52,a.ComplexDiv(b).getImagePart());
    }
}