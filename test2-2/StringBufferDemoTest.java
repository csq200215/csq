import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");
    //测试12个字符
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    //测试24个字符
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBuffer");
    //测试36个字符
    StringBuffer d = new StringBuffer("StringBufferStringBufferStringBufferStr");
    //测试39个字符
    @Test
    public void testcharAt() {
        assertEquals('S',a.charAt(0));
        assertEquals('e',b.charAt(10));
        assertEquals('f',c.charAt(20));
        assertEquals('B',d.charAt(30));
    }
    @Test
    public void testcapacity() {
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
        assertEquals(55,d.capacity());
    }
    @Test
    public void testindexOf() {
        assertEquals(0,a.indexOf("Str"));
        assertEquals(3,b.indexOf("ing"));
        assertEquals(6,c.indexOf("Buffer"));
        assertEquals(8,d.indexOf("ff"));
    }
    @Test
    public void testlength() {
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
        assertEquals(39,d.length());
    }

}