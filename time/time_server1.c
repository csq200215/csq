#include <netinet/in.h>    // for sockaddr_in
#include <sys/types.h>    // for socket
#include <sys/socket.h>    // for socket
#include <stdio.h>        // for printf
#include <stdlib.h>        // for exit
#include <string.h>        // for bzero
#include <unistd.h>        // for fork
#include <sys/signal.h> // for signal
#include <sys/wait.h>    // for wait
#include <time.h>
#include <arpa/inet.h>
#define HELLO_WORLD_SERVER_PORT    6666 
#define LENGTH_OF_LISTEN_QUEUE  20
#define BUFFER_SIZE 1024
void reaper(int sig)
{
    int status;
    //调用wait3读取子进程的返回值,使zombie状态的子进程彻底释放
    while(wait3(&status,WNOHANG,(struct rusage*)0) >=0)
        ;
}
int main(int argc, char **argv)
{
    //设置一个socket地址结构server_addr,代表服务器internet地址, 端口
    struct sockaddr_in server_addr;
    bzero(&server_addr,sizeof(server_addr)); //把一段内存区的内容全部设置为0
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(HELLO_WORLD_SERVER_PORT);

    time_t t;


    //创建用于internet的流协议(TCP)socket,用server_socket代表服务器socket
    int server_socket = socket(AF_INET,SOCK_STREAM,0);
    if( server_socket < 0)
    {
        printf("Create Socket Failed!\n");
        exit(1);
    }
    
    //把socket和socket地址结构联系起来
    if( bind(server_socket,(struct sockaddr*)&server_addr,sizeof(server_addr)))
    {
        printf("Server Bind Port : %d Failed!\n", HELLO_WORLD_SERVER_PORT); 
        exit(1);
    }
    
    //server_socket用于监听
    if ( listen(server_socket, LENGTH_OF_LISTEN_QUEUE) )
    {
        printf("Server Listen Failed!"); 
        exit(1);
    }
    //通知操作系统,当收到子进程的退出信号(SIGCHLD)时,执行reaper函数,释放zombie状态的进程
    (void)signal(SIGCHLD,reaper);
    
    while (1) //服务器端要一直运行
    {
        //定义客户端的socket地址结构client_addr
        struct sockaddr_in client_addr;
        socklen_t length = sizeof(client_addr);

        //接受一个到server_socket代表的socket的一个连接
        //如果没有连接请求,就等待到有连接请求--这是accept函数的特性
        //accept函数返回一个新的socket,这个socket(new_server_socket)用于同连接到的客户的通信
        //new_server_socket代表了服务器和客户端之间的一个通信通道
        //accept函数把连接到的客户端信息填写到客户端的socket地址结构client_addr中

        int new_server_socket = accept(server_socket,(struct sockaddr*)&client_addr,&length);
        if ( new_server_socket < 0)
        {
            printf("Server Accept Failed!\n");
            break;
        }
        int child_process_pid = fork(); //fork()后,子进程是主进程的拷贝
        //在主进程和子进程中的区别是fork()的返回值不同.
        if(child_process_pid == 0 )//如果当前进程是子进程,就执行与客户端的交互
        {
             t=time(NULL);
            close(server_socket); //子进程中不需要被复制过来的server_socket
            char buffer[BUFFER_SIZE];
            bzero(buffer, BUFFER_SIZE);
            //strcpy(buffer,"20181217实现");
            strcat(buffer,"\nhello,20181217\n"); //C语言字符串连接              
            
            //发送buffer中的字符串到new_server_socket,实际是给客户端
            send(new_server_socket,buffer,BUFFER_SIZE,0);
            printf("服务器实现者学号：20181217\n");
	        
            printf("当前时间： %s\n",ctime(&t));
            send(new_server_socket,(void *)&t,sizeof(time_t),0);//发送给客户端

            bzero(buffer,BUFFER_SIZE);
            //接收客户端发送来的信息到buffer中
            length = recv(new_server_socket,buffer,BUFFER_SIZE,0);
            if (length < 0)
            {
                printf("Server Recieve Data Failed!\n");
                exit(1);
            }
            printf("\n%s\n",buffer);
            //关闭与客户端的连接
            close(new_server_socket); 
            exit(0);         
        }
        else if(child_process_pid > 0)     //如果当前进程是主进程 
            close(new_server_socket);    //主进程中不需要用于同客户端交互的new_server_socket
    }
    //关闭监听用的socket
    close(server_socket);
    return 0;
}