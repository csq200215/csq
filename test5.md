1.
### ![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/090335_c434b24b_2237712.png "屏幕截图.png")
我们取一个16个字节的密钥 拼成 2018121789fda4ba，使用密钥进行加解密。
对应的16进制为0x32 0x30 0x31 0x38 0x31 0x32 0x31 0x37 0x 38 0x 39 f(0x66) d(0x64) a(0x61) 0x34 b(0x62) a(0x61)
### 采用sm4 - ecb 加密 （没有iv）
### ![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/112547_b2644af8_2237712.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/120450_96f16438_2237712.png "屏幕截图.png")
### 用openssl进行解密
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/120417_24b5a8f5_2237712.png "屏幕截图.png")
2.编程实现openssl 加解密

```
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
 

void tEVP_Encrypt()
{
unsigned char key[EVP_MAX_KEY_LENGTH];  //密钥
unsigned char iv[EVP_MAX_KEY_LENGTH];//初始化向量
EVP_CIPHER_CTX* en_ctx;//EVP算法上下文
en_ctx = EVP_CIPHER_CTX_new();

EVP_CIPHER_CTX* de_ctx;
de_ctx = EVP_CIPHER_CTX_new();

unsigned char out[1024], de_out[1024];//输出密文缓冲区
int outl, de_outlen;//密文长度
int outltmp, de_outlent;
const char* msg = "\x32\x30\x31\x38\x31\x32\x31\x37";//待加密的数据

int rv, de_rv;
int i;

//设置key和iv（可以采用随机数和可以是用户输入）
key[0]=0x32;
key[1]=0x30;
key[2]=0x31;
key[3]=0x38;
key[4]=0x31;
key[5]=0x32;
key[6]=0x31;
key[7]=0x37;
key[8]=0x38;
key[9]=0x39;
key[10]=0x66;
key[11]=0x64;
key[12]=0x61;
key[13]=0x34;
key[14]=0x62;
key[15]=0x61;

//初始化密码算法结构体
EVP_CIPHER_CTX_init(en_ctx);
//设置算法和密钥以
rv = EVP_EncryptInit_ex(en_ctx, EVP_sm4_ecb(), NULL, key, iv);


if (rv != 1)
{
        printf("Err\n");
        return;
}
//数据加密
rv = EVP_EncryptUpdate(en_ctx, out, &outl, (const unsigned char*)msg, strlen(msg));
if (rv != 1)
{
        printf("Err\n");
        return;
}
//结束数据加密，把剩余数据输出。
rv = EVP_EncryptFinal_ex(en_ctx, out + outl, &outltmp);
if (rv != 1)
{
        printf("Err\n");
        return;
}
outl = outl + outltmp;
printf("原文为:%s\n", msg);
//打印输出密文
printf("密文长度：%d\n密文16进制数据：\n", outl);
for (i = 0; i < outl; i++)
{
        printf("0x%02x ", out[i]);
}
printf("\n");

de_ctx = EVP_CIPHER_CTX_new();
EVP_CIPHER_CTX_init(en_ctx);

de_rv = EVP_DecryptInit_ex(de_ctx, EVP_sm4_ecb(), NULL, key, iv);
if (de_rv != 1) {
        printf("Err\n");
        return;
}
de_rv = EVP_DecryptUpdate(de_ctx, de_out, &de_outlen, (const unsigned char*)out, outl);
if (rv != 1) {
        printf("Err\n");
        return;
}
rv = EVP_DecryptFinal_ex(de_ctx, de_out + de_outlen, &de_outlent);
if (rv != 1) {
        printf("Err\n");
        return;
}
de_outlen += de_outlent;
printf("密文是：");
for (i = 0; i < outl; i++)
{
        printf("%c", out[i]);
}
printf("结束添加汉字\n");
printf("解密信息长度：%d\n解密信息数据：\n", outl);
for (i = 0; i < de_outlen; i++)
{
        printf("%c", de_out[i]);
}

}

int main()
{ 

        OpenSSL_add_all_algorithms();
        tEVP_Encrypt();
        return 0;
}

```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/112834_c2ecc6da_2237712.png "屏幕截图.png")
可知对比一下是非常正确的，啦啦啦~
### 3.对所有同学的8位学号加解密
####将所有同学的学号放进20181217.txt中

```
2018120120181202201812032018120420181205201812062018120720181208201812092018121020181211201812122018121320181214201812152018121620181217201812182018121920181220201812212018122220181223201812242018122520181226201812272018122820181229201812302018123120181232201812332018123420181235201813012018130220181303201813042018130520181306201813072018130820181309201813102018131120181312201813132018131420181315201813162018131720181318201813192018132020181321201813222018132320181324201813252018132620181327201813282018132920181330201813312018133220181333
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115010_fe453011_2237712.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115032_584b2daa_2237712.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115047_f95f95b8_2237712.png "屏幕截图.png")
openssl 加密：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115113_39beea95_2237712.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115338_e138b307_2237712.png "屏幕截图.png")
```
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
 

void tEVP_Encrypt()
{
	unsigned char key[EVP_MAX_KEY_LENGTH];	//密钥
unsigned char iv[EVP_MAX_KEY_LENGTH];//初始化向量
EVP_CIPHER_CTX* en_ctx;//EVP算法上下文
en_ctx = EVP_CIPHER_CTX_new();

EVP_CIPHER_CTX* de_ctx;
de_ctx = EVP_CIPHER_CTX_new();

unsigned char out[1024], de_out[1024];//输出密文缓冲区
int outl, de_outlen;//密文长度
int outltmp, de_outlent;
//const char* msg = "20181217";//待加密的数据
int rv, de_rv;
int i;
char msg[10000] = "";                          
        FILE *file;
        char line[32];
        char *ret;
        file = fopen("20181217.txt", "r");              // 打开文件
        if (!file) {                                            // 判断文件是否打开失败
                printf("文件打开失败!\n");
                return 1;
        }

        /*
        while (1) {
                ret = fgets(line, sizeof(line), file);
                if (!ret) {
                        break;
                }
                printf("%s", line);
        }
        */

        while (ret = fgets(line, sizeof(line), file)) {         // 从文件中读取一行并
                //printf("%s", line);
                strcat(msg,line);
                //printf("hello!!!chenchen");
                // line就是读取到的一行字符串，根据自己的需要进行操作字符串即可
        }

    printf("20181217.txt中的内容为\n%s\n",msg);
        fclose(file);
//设置key（可以采用随机数和可以是用户输入）
key[0]=0x32;
key[1]=0x30;
key[2]=0x31;
key[3]=0x38;
key[4]=0x31;
key[5]=0x32;
key[6]=0x31;
key[7]=0x37;
key[8]=0x38;
key[9]=0x39;
key[10]=0x66;
key[11]=0x64;
key[12]=0x61;
key[13]=0x34;
key[14]=0x62;
key[15]=0x61;
//初始化密码算法结构体
EVP_CIPHER_CTX_init(en_ctx);
//设置算法和密钥以
rv = EVP_EncryptInit_ex(en_ctx, EVP_sm4_ecb(), NULL, key, iv);


if (rv != 1)
{
	printf("Err\n");
	return;
}
//数据加密
rv = EVP_EncryptUpdate(en_ctx, out, &outl, (const unsigned char*)msg, strlen(msg));
if (rv != 1)
{
	printf("Err\n");
	return;
}
//结束数据加密，把剩余数据输出。
rv = EVP_EncryptFinal_ex(en_ctx, out + outl, &outltmp);
if (rv != 1)
{
	printf("Err\n");
	return;
}
outl = outl + outltmp;
//printf("原文为:%s\n", msg);
//打印输出密文
printf("密文长度：%d\n密文16进制数据：\n", outl);
for (i = 0; i < outl; i++)
{
	printf("0x%02x ", out[i]);
}
printf("\n");

de_ctx = EVP_CIPHER_CTX_new();
EVP_CIPHER_CTX_init(en_ctx);

de_rv = EVP_DecryptInit_ex(de_ctx, EVP_sm4_ecb(), NULL, key, iv);
if (de_rv != 1) {
	printf("Err\n");
	return;
}
de_rv = EVP_DecryptUpdate(de_ctx, de_out, &de_outlen, (const unsigned char*)out, outl);
if (rv != 1) {
	printf("Err\n");
	return;
}
rv = EVP_DecryptFinal_ex(de_ctx, de_out + de_outlen, &de_outlent);
if (rv != 1) {
	printf("Err\n");
	return;
}
de_outlen += de_outlent;
printf("密文是：");
for (i = 0; i < outl; i++)
{
	printf("%c", out[i]);
}
printf("结束添加汉字\n");
printf("解密信息长度：%d\n解密信息数据：\n", outl);
for (i = 0; i < de_outlen; i++)
{
	printf("%c", de_out[i]);
}
	
}

int main()
{ 
	
	OpenSSL_add_all_algorithms();
	tEVP_Encrypt();
	return 0;
}


```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115138_85518233_2237712.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/115153_46711cf7_2237712.png "屏幕截图.png")
认真和openssl加密结果对比是正确的 ~~~