![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/094441_3919b641_2237712.png "屏幕截图.png")

```
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
void tDigest()
{
	unsigned char sm3_value[EVP_MAX_MD_SIZE];	//保存输出的摘要值的数组
	int sm3_len, i;
	EVP_MD_CTX *sm3ctx;							//EVP消息摘要结构体
	sm3ctx = EVP_MD_CTX_new();
	char msg1[] = "20181217";				//待计算摘要的消息1	
	//char msg2[] = "Test Message2";				//待计算摘要的消息2
	
	EVP_MD_CTX_init(sm3ctx);					//初始化摘要结构体 
	EVP_DigestInit_ex(sm3ctx, EVP_sm3(), NULL);	//设置摘要算法和密码算法引擎，这里密码算法使用MD5，算法引擎使用OpenSSL默认引擎即软算法
	EVP_DigestUpdate(sm3ctx, msg1, strlen(msg1));//调用摘要UpDate计算msg1的摘要
	//EVP_DigestUpdate(sm3ctx, msg2, strlen(msg2));//调用摘要UpDate计算msg2的摘要	
	EVP_DigestFinal_ex(sm3ctx, sm3_value, &sm3_len);//摘要结束，输出摘要值	
	EVP_MD_CTX_reset(sm3ctx);						//释放内存
	
	printf("原始数据%s的摘要值为:\n",msg1);
	for(i = 0; i < sm3_len; i++) 
	{
		printf("0x%02x ", sm3_value[i]);
	}
	printf("\n");
}
int main()
{ 
	OpenSSL_add_all_algorithms();
	tDigest();
	return 0;
}

```
### 对比一下，可知运行结果很正确~~
![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/092537_0571dc90_2237712.png "屏幕截图.png")
### 使用OpenSSL编程对计算内容为"所有同学的8位学号"的文件的SM3摘要值，提交代码和运行结果截图。
### 首先把所有同学的8位学号全部放进文件hello.txt中：
20181201 20181202 20181203 20181204 20181205 20181206 20181207 20181208 20181209 20181210 20181211 20181212 20181213 20181214 20181215 20181216 20181217 20181218 20181219 20181220 20181221 20181222 20181223 20181224 20181225 20181226 20181227 20181228 20181229 20181230 20181231 20181232 20181233 20181234 20181235 20181301 20181302 20181303 20181304 20181305 20181306 20181307 20181308 20181309 20181310 20181311 20181312 20181313 20181314 20181315 20181316 20181317 20181318 20181319 20181320 20181321 20181322 20181323 20181324 20181325 20181326 20181327 20181328 20181329 20181330 20181331 20181332 20181333
![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/102956_95c44f0b_2237712.png "屏幕截图.png")

```
//计算所有同学的八位学号的sm3摘要值
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
void tDigest()
{
	unsigned char sm3_value[EVP_MAX_MD_SIZE];	//保存输出的摘要值的数组
	int sm3_len, i;
	EVP_MD_CTX *sm3ctx;							//EVP消息摘要结构体
	sm3ctx = EVP_MD_CTX_new();
	char msg1[10000] = "";				//待计算摘要的消息1
	FILE *file;
	char line[32];
	char *ret;
	file = fopen("20181217.txt", "r");		// 打开文件
	if (!file) {						// 判断文件是否打开失败
		printf("文件打开失败!\n");
		return 1;
	}

	/*
	while (1) {
		ret = fgets(line, sizeof(line), file);
		if (!ret) {
			break;
		}
		printf("%s", line);
	}
	*/

	while (ret = fgets(line, sizeof(line), file)) {		// 从文件中读取一行并
		//printf("%s", line);
		strcat(msg1,line);
		//printf("hello!!!chenchen");
		// line就是读取到的一行字符串，根据自己的需要进行操作字符串即可
	}

    printf("20181217.txt中的内容为\n%s\n",msg1);
	fclose(file);

	EVP_MD_CTX_init(sm3ctx);					//初始化摘要结构体
	EVP_DigestInit_ex(sm3ctx, EVP_sm3(), NULL);	//设置摘要算法和密码算法引擎，这里密码算法使用MD5，算法引擎使用OpenSSL默认引擎即软算法
	EVP_DigestUpdate(sm3ctx, msg1, strlen(msg1));//调用摘要UpDate计算msg1的摘要

	EVP_DigestFinal_ex(sm3ctx, sm3_value, &sm3_len);//摘要结束，输出摘要值
	EVP_MD_CTX_reset(sm3ctx);						//释放内存

	printf("原始文件20181217.txt的摘要值为:\n");
	for(i = 0; i < sm3_len; i++)
	{
		printf("0x%02x ", sm3_value[i]);
	}
	printf("\n");
}
int main()
{
	OpenSSL_add_all_algorithms();
	tDigest();
	return 0;
}

```
### 运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/103137_87873130_2237712.png "屏幕截图.png")
### 对比一下openssl的命令，可以知道运行结果是正确的hhh
![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/103357_92349c28_2237712.png "屏幕截图.png")