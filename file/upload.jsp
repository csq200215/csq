<!-- 图片文件上传工具 -->
<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.util.*,com.jspsmart.*" errorPage="" %>
<%@ page import="com.jspsmart.upload.*,java.text.SimpleDateFormat;" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
<title>文件上传</title>
</head> 

<body>
<%          
            Date date = new Date();
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			
			String DiskUrl = request.getSession().getServletContext().getRealPath("/");
			SmartUpload su = new SmartUpload();
			
			su.initialize(pageContext);		
				
			try{
			 su.setMaxFileSize(1000*1024*5);
			 su.setTotalMaxFileSize(1000*1024*5);
				
				
			su.upload();
	
			com.jspsmart.upload.File file = null;
			
			String filename ="";
			
			for (int i = 0; i < su.getFiles().getCount(); i++) {
			
			
				file = su.getFiles().getFile(i);
				
   				filename = format.format(date)+"F"+"."+file.getFileExt();
								
				if (!file.isMissing()){
				file.saveAs(DiskUrl+"\\upload\\"+filename,su.SAVE_PHYSICAL);
				//file.saveAs("/slave/"+filename,su.SAVE_VIRTUAL);
				}					
			}
			out.print("<script>");
			out.print("alert('上传成功');");
			out.print("window.opener.document.getElementById('fileName').value='"+filename+"';");
			out.print("window.close();");
            out.print("</script>");
			  }catch(Exception e){
			 out.print("<script>");
			 out.print("alert('对不起，附件过大，请裁剪后再上传！');");	
			 out.print("window.close();");	
			  out.print("</script>");	
			}
		%>
</body>
</html>
