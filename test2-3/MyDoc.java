abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    @Override
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Short extends Data{
    short value;
    Short(){
        value=29;
    }
    @Override
    public void DisplayValue(){
        System.out.println(value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    @Override
    public Data CreateDataObject(){
        return new Integer();
    }
}
class ShortFactory extends Factory{
    @Override
    public Data CreateDataObject(){
        return new Short();
    }
}
class Document {
    Data p;
    Document(Factory pf){
        p = pf.CreateDataObject();
    }
    public void DisplayData(){
        p.DisplayValue();
    }
}
/**
 * @author cindy
 */
public class MyDoc {
    static Document a;
    static Document b;
    public static void main(String[] args) {
        a = new Document(new IntFactory());
        a.DisplayData();
        b=new Document(new ShortFactory());
        b.DisplayData();

    }
}