utils.c
```
#include<stdio.h>
#include"utils.h"
#include<string.h>
int Hex2Char(int fromi,char *toc)
{
    if(fromi>=0&&fromi<=9)
    {
	    *toc= fromi+'0';
    } 
    else if(fromi>=10&&fromi<=15)
    {
	    *toc = fromi+'A'-10;
    }
    else
    {
        printf("不在范围内!");
    }
    return 0;
}
int Char2Hex(char fromc,int *toi)
{
	if(fromc>='0'&& fromc<='9')
	{
             *toi= fromc-'0';
	}
	else if(fromc>='A'&& fromc<='F')
	{
             *toi= fromc-'A'+10;
	
        }
        else{
             printf("不在范围内");
        }
	return 0;
}
```
utils.h

```
#ifndef UTILS_H
#define UTILS_H

int Hex2Char(int fromi,char *toc);//16进制数转换成16进制的字符；
int Char2Hex(char fromc,int *toi);//16进制的字符转化成16进制数；
int BitStr2ByteArr(char *bs,char *ba);//位数组转到字节数组；
int ByteArr2BitStr(char *ba,char *bs);//字节数组转化为位数组；
int INT2ByteArr(int i,char *ba);//整型数转化为字节数组
int ByteArr2INT(char *ba,int *i);//字节数组转化为整型

#endif
```

main.c

```
#include<stdio.h>
#include"utils.h"
int main(){
  
  unsigned char * c;
  int n=0;
  int i=0;
  for(i=0;i<=15;i++)
  {
      Hex2Char(n, &c);
      printf("c=%c\n",c);
      n=n+1;
  }
  char m='A';
  int y='0';
  int k;
  for(i=0;i<=9;i++)
  {
     Char2Hex(y, &k);
     printf("k=%d\n",k);
     y=y+1;
  }
  for(i=0;i<=5;i++)
  {
     Char2Hex(m, &k);
     printf("k=%d\n",k);
     m=m+1;
  }
  printf("测试几个超出范围的数字和16进制字符,比如17和G");
  n=17;
  y='G';
  Hex2Char(n, &c);
  Char2Hex(y, &k);
  return 0;
  
  return 0;
}
```
#### 运行截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/092029_1b4f91b8_2237712.png "屏幕截图.png")