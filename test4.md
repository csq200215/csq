utils.c

```
#include<stdio.h>
#include"utils.h"
#include<string.h>
int Hex2Char(int fromi,char *toc)
{
    if(fromi>=0&&fromi<=9)
    {
	    *toc= fromi+'0';
    } 
    else if(fromi>=10&&fromi<=15)
    {
	    *toc = fromi+'A'-10;
    }
    else
    {
        printf("输入的16进制数据不正确！");
     }
    return 0;
}
int Char2Hex(char fromc,int *toi)
{
	if(fromc>='0'&& fromc<='9')
	{
             *toi= fromc-'0';
	}
	else if(fromc>='A'&& fromc<='F')
	{
             *toi= fromc-'A'+10;
	
        }
        else 
        {
            printf("输入的16进制字符不正确");
        }
	return 0;
}

int ByteArr2BitStr(char *ba,char *bs)
{
    int i,j;
    int len;
    int k;
    int a;
    len = strlen(ba);
    for(j=0;j<len;j++)
    {
        Char2Hex(ba[j],&a);
        for(i=0;i<4;i++)//4位 
        {
            
            bs[3*(j+1)-i+j] = a%2+'0';
            a=a/2;
        }
    }
    bs[4*len]='\0';
}
//将bits数组转化为字节数组
int BitStr2ByteArr(char *bs,char *ba){
   int len;
   len = strlen(bs);
   char string[100];
   int k;
   int i;
   int j;
   int sum=0;
   if(len%4==1)
   {
     strcpy(string,"000");
     strcat(string,bs);
     k=len/4+1;

   }
   else if(len%4==2)
   {
     strcpy(string,"00");
     strcat(string,bs);
     k=len/4+1;
   }
    else if(len%4==3)
   { 
      strcpy(string,"0");;
      strcat(string,bs);
      k=len/4+1;
   }
   else
   {
       k=len/4;
       strcpy(string,bs);
   }
   //printf("%s\n",string);
   for(i=0;i<k;i++)
   { 
       
       for(j=0;j<4;j++)
       { 
           sum=sum*2;
           sum=sum+(string[4*i+j]-'0');
           //printf("%d\n",sum);
           

       }
       Hex2Char(sum,&ba[i]);
       //printf("%c\n",ba[i]);
       sum=0;   
   }
   ba[i]='\0';

}

```
utils.h

```
#ifndef UTILS_H
#define UTILS_H

int Hex2Char(int fromi,char *toc);//16进制数转换成16进制的字符；
int Char2Hex(char fromc,int *toi);//16进制的字符转化成16进制数；
int BitStr2ByteArr(char *bs,char *ba);//位数组转到字节数组；
int ByteArr2BitStr(char *ba,char *bs);//字节数组转化为位数组；
int INT2ByteArr(int i,char *ba);//整型数转化为字节数组
int ByteArr2INT(char *ba,int *i);//字节数组转化为整型

#endif

```
main.c

```
#include<stdio.h>
#include"utils.h"
int main(){
  //把字节数组转化为bits数组
  char bytestr[100];
  char  bitsstr[100];
  printf("请输入一串16进制的字节串");
  scanf("%s",bytestr);
  ByteArr2BitStr(bytestr,bitsstr);
  printf("计算出的bit串为\n");
  printf("%s\n",bitsstr);

  //把bits数组转化为字节数组
  char bytestr_1[100];
  char  bitsstr_1[100];
  printf("请输入一串01bit串:");
  scanf("%s",bitsstr_1);
  printf("将bit串转化为字节数组\n");
  BitStr2ByteArr(bitsstr_1,bytestr_1);
  printf("打印出字节数组:\n");
  printf("%s\n",bytestr_1);
  return 0;
}
```
### 运行截图:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/084811_5a555aae_2237712.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/090037_edf398a2_2237712.png "屏幕截图.png")